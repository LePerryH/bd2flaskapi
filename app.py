from flask import Flask, jsonify, request
from neo4j import GraphDatabase
import os

#load_dotenv("config.env")

URI = os.environ.get("NEO4J_URI")
AUTH = (os.environ.get("NEO4J_USERNAME"), os.environ.get("NEO4J_PASSWORD"))


app = Flask(__name__)

# driver = GraphDatabase.driver(URI, auth=AUTH, database="neo4j")


### FUNCTIONS ###

def add_employee(name, position, department):
    with GraphDatabase.driver(URI, auth=AUTH) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (employee:Employee {name: $name}) RETURN employee",
                name=name
            )
            existing_employee = result.single()

            if existing_employee:
                return "taki pracownik juz istnieje."

            result = session.run(
                "CREATE (employee:Employee {name: $name, position: $position})-[:WORKS_IN]->(department:Department {name: $department}) RETURN employee",
                name=name,
                position=position,
                department=department
            )

            created_employee = result.single()

            created_employee_data = dict(created_employee["employee"])

            return created_employee_data

def get_employees(sort_by=None, filter_by=None):
    with GraphDatabase.driver(URI, auth=AUTH) as driver:
        with driver.session() as session:
            cypher_query = "MATCH (employee:Employee)"

            if filter_by:
                cypher_query += f" WHERE employee.name CONTAINS '{filter_by}'"

            cypher_query += " RETURN employee"

            if sort_by:
                cypher_query += f" ORDER BY employee.{sort_by}"

            result = session.run(cypher_query)
      
            employees = [dict(record["employee"]) for record in result]

            return employees

def is_unique_employee_name(tx, name):
    query = "MATCH (e:Employee) WHERE e.name = $name RETURN e"
    result = tx.run(query, name=name)
    return result.single() is None

def add_employee(tx, name, position, department_id):
    query = (
        "CREATE (e:Employee {name: $name, position: $position}) "
        "WITH e MATCH (d:Department {dept_id: $department_id}) "
        "CREATE (e)-[:WORKS_IN]->(d)"
    )
    tx.run(query, name=name, position=position, department_id=department_id)

# def add_employee(tx, name, position, department):
#     exits = f"MATCH (m:Employee) WHERE m.name = '{name}' RETURN m"
#     result = tx.run(exits).data()
#     if not result:
#         query = "MATCH (d:Department {name: $department}) CREATE (e:Employee {name: $name, surname: $surname, position: $position})-[:WORKS_IN]->(d)"
#         tx.run(query, name=name,
#                position=position, department=department)
#         return True
#     else:
#         return False

def delete_employee(tx, id, new_id=None):
    if new_id:
        query = "MATCH (n:Employee) WHERE id(n) = $internal_id OPTIONAL MATCH (n)-[r:WORKS_IN]->(d:Department) OPTIONAL MATCH (n)-[r2:MANAGES]->(d2:Department) WITH n, d, d2 MATCH (n2:Employee) WHERE id(n2) = $new_internal_id MERGE (n2)-[:MANAGES]->(d2) DETACH DELETE n"
        tx.run(query, internal_id=id, new_internal_id=new_id)
    else:
        query = "MATCH (n:Employee) WHERE id(n) = $internal_id OPTIONAL MATCH (n)-[r:WORKS_IN]->(d:Department) OPTIONAL MATCH (n)-[r2:MANAGES]->(d2:Department) DETACH DELETE n,d2"
        tx.run(query, internal_id=id)

def update_employee(tx, id, name=None, position=None, department=None):
    if name:
        query = "MATCH (n:Employee) WHERE id(n) = $internal_id SET n.name = $name"
        tx.run(query, internal_id=id, name=name)
    if position:
        query = "MATCH (n:Employee) WHERE id(n) = $internal_id SET n.position = $position"
        tx.run(query, internal_id=id, position=position)
    if department:
        query = "MATCH (n:Employee) WHERE id(n) = $internal_id MATCH (n)-[r:WORKS_IN]->(d:Department) MATCH (d2:Department {name: $department}) DELETE r MERGE (n)-[:WORKS_IN]->(d2)"
        tx.run(query, internal_id=id, department=department)

def query_departments(sort_by=None, filter_by=None):
    with GraphDatabase.driver(URI, auth=AUTH).session() as session:
        query = "MATCH (d:Department) "
        if filter_by:
            query += f"WHERE d.name CONTAINS '{filter_by}' "
        query += "RETURN d "
        if sort_by:
            query += f"ORDER BY d.{sort_by}"
        result = session.run(query)
        return result.data()

def get_department_employees(department_id):
    with GraphDatabase.driver(URI, auth=AUTH).session() as session:
        # Cypher query to retrieve all employees of a specific department
        query = (
            f"MATCH (d:Department)-[:WORKS_IN]->(e:Employee) "
            f"WHERE ID(d) = {department_id} "
            "RETURN e"
        )
        result = session.run(query)
        return result.data()



### ROUTES ###
@app.route("/employees2", methods=['GET'])
def create_account():
    with GraphDatabase.driver(URI, auth=AUTH) as driver:
        driver.verify_connectivity()
        records, summary, keys = driver.execute_query(
            "MATCH (employee:Employee) RETURN employee",
            database_="neo4j",
        )

        for employee in records:
            print(employee)

        print("The query `{query}` returned {records_count} records in {time} ms.".format(
            query=summary.query, records_count=len(records),
            time=summary.result_available_after,
        ))
    return jsonify({"message": f"{records}"}), 201

@app.route('/employees', methods=['GET'])
def get_employees_endpoint():

    sort_by = request.args.get('sort_by')
    filter_by = request.args.get('filter_by')

    employees = get_employees(sort_by=sort_by, filter_by=filter_by)

    return jsonify({"employees": employees}), 200

@app.route("/employees", methods=["POST"])
def add_employee_endpoint():
    try:
        data = request.json
        name = data["name"]
        position = data["position"]
        department_id = data["department_id"]

        # Check if the employee name is unique
        with GraphDatabase.driver(URI, auth=AUTH) as driver:
            with driver.session() as session:
                if session.read_transaction(is_unique_employee_name, name):
                    # If the name is unique, add the employee
                    with driver.session() as session:
                        session.write_transaction(add_employee, name, position, department_id)
                    return jsonify({"message": "Employee added successfully"}), 201
                else:
                    return jsonify({"error": "Employee name must be unique"}), 400
    except KeyError:
        return jsonify({"error": "Missing required data"}), 400

@app.route('/employees/<int:id>', methods=['DELETE'])
def delete_employee_route(id):
    if not request.json:
        with GraphDatabase.driver(URI, auth=AUTH) as driver:
            with driver.session() as session:
                session.write_transaction(delete_employee, id)
            response = {'message': 'Employee deleted'}
        return jsonify(response)
    else:
        new_id = request.json.get('new_id')
        with GraphDatabase.driver(URI, auth=AUTH) as driver:
            with driver.session() as session:
                session.write_transaction(delete_employee, id, new_id)
            response = {'message': 'Employee deleted'}
        return jsonify(response)
    
@app.route('/employees/<int:id>', methods=['PUT'])
def update_employee_route(id):

    name = request.json.get('name')
    position = request.json.get('position')
    department = request.json.get('department')

    with GraphDatabase.driver(URI, auth=AUTH) as driver:
        with driver.session() as session:
            session.write_transaction(
                update_employee, id, name, position, department)
        response = {'message': 'Employee updated'}
    return jsonify(response)

@app.route('/departments', methods=['GET'])
def departments():

    sort_by = request.args.get('sort_by')
    filter_by = request.args.get('filter_by')

    departments_data = query_departments(sort_by=sort_by, filter_by=filter_by)
    return jsonify({'departments': departments_data})

@app.route('/departments/<int:department_id>/employees', methods=['GET'])
def department_employees(department_id):
    
    employees_data = get_department_employees(department_id)

    return jsonify({'employees': employees_data})

